#!/bin/sh

# Author: Gokhan USTUNER
# Relational operators
# Script gets two arguments and compare their values

message="None"

# Check the equality
if [ $1 -eq $2 ] 
then
  message="$1 is equal to $2"
# Check the not equality
elif [ $1 -ne $2 ]
then
  message="$1 is not equal to $2"
# Check if $1 is greater than $2
elif [ $1 -gt $2 ] 
then
  message="$1 is greater than $2"
# Check if $1 less than $2
elif [ $1 -lt $2 ]
then
  message="$1 is less than $2"
# Check if $1 is greater than or equal to $2
elif [ $1 -ge $2 ]
then
  message="$1 is greater than or equal to $2"
# Check if $1 is less than or equal to $2
elif [ $1 -le $2 ]
then
  message="$1 is less than or equal to $2"
fi

echo $message
