#!/bin/sh

# Author: Gokhan USTUNER
# Artihmetic operators for Bourne shell
# Script gets two arguments and makes arithmetic processes

NUMBERS=($1 $2)

addition=`expr ${NUMBERS[0]} + ${NUMBERS[1]}`
subtraction=`expr ${NUMBERS[1]} - ${NUMBERS[0]}`
division=`expr ${NUMBERS[0]} / ${NUMBERS[1]}`
multiplication=`expr ${NUMBERS[0]} \* ${NUMBERS[1]}`
modulus=`expr ${NUMBERS[0]} % ${NUMBERS[1]}`
equality="${NUMBERS[0]} is not equal to ${NUMBERS[1]}"
notequality="${NUMBERS[0]} is equal to ${NUMBERS[1]}"

if [ ${NUMBERS[0]} == ${NUMBERS[1]} ]
then
  equality="${NUMBERS[0]} is equal to ${NUMBERS[1]}"
fi

if [ ${NUMBERS[0]} != ${NUMBERS[1]} ]
then
  notequality="${NUMBERS[0]} is not equal to ${NUMBERS[1]}"
fi

# Check the values...

echo $addition
echo $subtraction
echo $division
echo $multiplication
echo $modulus
echo $equality
echo $notequality
