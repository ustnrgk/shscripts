#!/bin/sh

# Author: Gokhan USTUNER
# Command Substitution

DATE=`date`
echo "Date is $DATE"

USERS=`who | wc -l`
echo "Logged in user are $USERS"

UP=`date ; uptime`
echo "Uptime is $UP"
