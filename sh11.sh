#!/bin/sh

# Author: Gokhan USTUNER
# File Test Operators

if [ $1 ]
then
  filename=$1
  
  if [ -e $filename ]
  then
    if [ -b $filename ]
    then
      echo "Block special file."
    fi
    
    if [ -c $filename ]
    then
      echo "Character special file."
    fi

    if [ -d $filename ]
    then
      echo "A directory."
    fi

    if [ -f $filename ]
    then
      echo "An ordinary file."
    fi

    if [ -g $filename ]
    then
      echo "Group ID is set."
    fi

    if [ -k $filename ]
    then
      echo "File has sticky bit set."
    fi

    if [ -p $filename ]
    then
      echo "File is a named pipe."
    fi

    if [ -t $filename ]
    then
      echo "File descriptor is open and associated with terminal."
    fi

    if [ -u $filename ]
    then
      echo "File has its Set User Id bit set."
    fi

    if [ -r $filename ]
    then
      echo "File is readable."
    fi

    if [ -w $filename ]
    then
      echo "File is writable."
    fi

    if [ -x $filename ]
    then
      echo "File is executable."
    fi

    if [ -s $filename ]
    then
      echo "File's size is greater than 0."
    fi

    if [ -e $filename ]
    then
      echo "File exists."
    fi
  else
    echo "File \"$filename\" does not exists."
  fi
else
  echo "Script must get 1 argument filename."
fi
