#!/bin/sh

# Author: Gokhan USTUNER
# Loop Controls

x=0

until [ $x -gt 10 ]
do
  echo $x
  x=`expr $x + 1`
done
