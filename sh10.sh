#!/bin/sh

# Author: Gokhan USTUNER
# String Operators
# Script gets two string arguments.

if [ $1 = $2 ]
then
  echo "$1 is equal to $2"
else
  echo "$1 is not equal to $2"
fi

if [ $1 != $2 ]
then
  echo "$1 is not equal to $2"
else
  echo "$1 is equal to $2"
fi

if [ -z $1 ]
then
  echo "$1 has zero length."
fi

if [ -z $2 ]
then
  echo "$2 has zero length." 
fi

if [ -n $1 ]
then
  echo "$1 length longer than zero." 
fi

if [ -n $2 ]
then
  echo "$2 length longer than zero." 
fi

if [ $1 ]
then
  echo "$1 is str"
else
  echo "$1 is not str" 
fi

if [ $2 ]
then
  echo "$2 is str"
else
  echo "$2 is not str"
fi
