#!/bin/sh

# Author: Gokhan USTUNER
# Boolean operators
# Script gets two arguments and compares them.

if [ $1 != $2 ]
then
  echo "$1 is not equal to $2"
else
  echo "$1 is equal to $2"
fi

if [ $1 -lt $2 -o $1 -eq $2 ]
then
 echo "$1 is less than $2 or $1 is equal to $2"
else
  echo "Neither $1 is less than $2 nor $1 is equal to $2"
fi

if [ $1 -gt $2 -a $1 -ne $2 ]
then
  echo "$1 is greater than $2 and $1 is not equal to $2"
else
  echo "$1 is less than $2 or $1 is equal to $2"
fi
# End of file
